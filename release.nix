let
  pkgs = import <nixpkgs> {};

  jobs = rec {
    tarball =
      pkgs.releaseTools.sourceTarball {
        name = "hydra-test-tarball";
        src = ./.;
        buildInputs = (with pkgs; [ emacs]);
      };
  };
in
  jobs
