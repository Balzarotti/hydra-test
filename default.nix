with import <nixpkgs> { };
stdenv.mkDerivation rec {

  name = "hydra-test-${version}";
  version = "0.0.1";
  src = ./.;

  buildInputs = [ emacs ];
  buildPhase = '' # -l ./.emacs.d/init.el
    emacs -q \
    notes.org \
    --batch \
    -f org-html-export-to-html \
    --kill'';
  installPhase = ''
    mkdir -p $out
    cp notes.html $out/
    cp -r styles $out/
  '';
}
